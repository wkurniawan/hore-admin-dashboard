// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

var markers = [];

function initAutocomplete() 
{
    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: {lat: Number(getLat), lng: Number(getLng)},
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    addMarker(map.center, 'Click Generated Marker', map);
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    // map.addListener('bounds_changed', function() {
    //     searchBox.setBounds(map.getBounds());
    // });

    // map.addListener('click', function(event) {
    //     deleteMarkers();
    //     addMarker(event.latLng, 'Click Generated Marker', map);
    // });

    // [START region_getplaces]
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() 
    {
        var places = searchBox.getPlaces();

        if (places.length == 0) 
        {
            return;
        }

        deleteMarkers();
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) 
        {
            addMarker(place.geometry.location, 'Default Marker', map);

            if (place.geometry.viewport) 
            {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } 
            else 
            {
                bounds.extend(place.geometry.location);
            }
        });
        google.maps.event.addListener(map, 'dragstart', function(event) {
            //infowindow.open(map,marker);
            //addMarker(event.latlngPos, 'Click Generated Marker', map);
            var lat, lng, address;
                  
        });

        map.fitBounds(bounds);
    });
    // [END region_getplaces]
}

function addMarker(latlng,title,map) 
{
    var marker = new google.maps.Marker({

        position: latlng,
        map: map,
        title: title,
        icon:'map-red.png',
        draggable:false
        // animation: google.maps.Animation.DROP
    });
    
    // document.getElementById('lat').value = marker.position.lat();
    // document.getElementById('lng').value = marker.position.lng();
    markers.push(marker);

    // google.maps.event.addListener(marker,'drag',function(event) {
    //     document.getElementById('lat').value = event.latLng.lat();
    //     document.getElementById('lng').value= event.latLng.lng();
    // });

    // google.maps.event.addListener(marker,'dragend',function(event) {
    //     document.getElementById('lat').value = event.latLng.lat();
    //     document.getElementById('lng').value = event.latLng.lng();
    // });

    // google.maps.event.addListener(markers,'click',function(event) {
    //     document.getElementById('lat').value = event.latLng.lat();
    //     document.getElementById('lng').value = event.latLng.lng();
    // });

}
function setMapOnAll(map) 
{
    for (var i = 0; i < markers.length; i++) 
    {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() 
{
    setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() 
{
    setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() 
{
    clearMarkers();
    markers = [];
}